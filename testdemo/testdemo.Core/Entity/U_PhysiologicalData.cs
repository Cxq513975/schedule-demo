﻿// -----------------------------------------------------------------------------
// Generate By Furion Tools v4.8.7
// -----------------------------------------------------------------------------

using Furion.DatabaseAccessor;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;

namespace testdemo.Core;

public partial class U_PhysiologicalData : IEntity<MasterDbContextLocator>, IEntityTypeBuilder<U_PhysiologicalData, MasterDbContextLocator>
{
    public string ID { get; set; }

    public double? Hr { get; set; }

    public double? Pulse { get; set; }

    public double? NumberBreaths { get; set; }

    public double? SystolicPressure { get; set; }

    public double? DiastolicPressure { get; set; }

    public double? InvasiveSystolicPressure { get; set; }

    public double? InvasiveDiastolicPressure { get; set; }

    public double? Temperature { get; set; }

    public double? EtCO2 { get; set; }

    public double? SpO2 { get; set; }

    public DateTime? CreateTime { get; set; }

    public int? State { get; set; }

    public string RoomName { get; set; }

    public int? DepartmentID { get; set; }

    public string TidalVolumePEAK { get; set; }

    public double? CVP { get; set; }

    public int? ECG { get; set; }

    public string MainID { get; set; }

    public double? FiO2 { get; set; }

    public double? QdPressure { get; set; }

    public double? BIS { get; set; }

    public double? JsValue { get; set; }

    public string Custom1 { get; set; }

    public string Custom2 { get; set; }

    public string Custom3 { get; set; }

    public string Custom4 { get; set; }

    public string ECGstr { get; set; }

    public string mzpm { get; set; }

    public double? VT { get; set; }

    public double? Peak { get; set; }

    public void Configure(EntityTypeBuilder<U_PhysiologicalData> entityBuilder, DbContext dbContext, Type dbContextLocator)
    {
        entityBuilder.HasKey(e => e.ID).HasName("PK_L_PhysiologicalData");

        entityBuilder.HasIndex(e => e.CreateTime, "IX_U_PhysiologicalData");

        entityBuilder.HasIndex(e => e.RoomName, "IX_U_PhysiologicalData_1");

        entityBuilder.Property(e => e.ID).HasMaxLength(50);
        entityBuilder.Property(e => e.CreateTime).HasColumnType("datetime");
        entityBuilder.Property(e => e.Custom1).HasMaxLength(10);
        entityBuilder.Property(e => e.Custom2).HasMaxLength(10);
        entityBuilder.Property(e => e.Custom3).HasMaxLength(10);
        entityBuilder.Property(e => e.Custom4).HasMaxLength(10);
        entityBuilder.Property(e => e.ECGstr).HasMaxLength(50);
        entityBuilder.Property(e => e.MainID).HasMaxLength(50);
        entityBuilder.Property(e => e.RoomName).HasMaxLength(50);
        entityBuilder.Property(e => e.State).HasDefaultValueSql("((0))");
        entityBuilder.Property(e => e.TidalVolumePEAK).HasMaxLength(50);
        entityBuilder.Property(e => e.mzpm).HasMaxLength(50);
    }
}
