﻿using Furion.DatabaseAccessor;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using testdemo.Core.Locator;

namespace testdemo.Core.Entity
{
    public partial class ICU_LisInfo : IEntity<MasterDbContextLocator,IcuDbContextLocator>, IEntityTypeBuilder<ICU_LisInfo, MasterDbContextLocator,IcuDbContextLocator>
    {
        public string F_Id { get; set; }

        /// <summary>
        /// 患者编号
        /// </summary>
        public string PatientID { get; set; }

        /// <summary>
        /// 住院号
        /// </summary>
        public string InpNo { get; set; }

        /// <summary>
        /// 姓名
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 性别
        /// </summary>
        public string Sex { get; set; }

        /// <summary>
        /// 检验项目
        /// </summary>
        public string ItemName { get; set; }

        /// <summary>
        /// 检验项目编码
        /// </summary>
        public string ItemCode { get; set; }

        /// <summary>
        /// 参照值
        /// </summary>
        public string StdValue { get; set; }

        /// <summary>
        /// 具体检验项
        /// </summary>
        public string TestItemName { get; set; }

        /// <summary>
        /// 具体检验编码
        /// </summary>
        public string TestItemCode { get; set; }

        public string Result { get; set; }

        public string ResultUnit { get; set; }

        /// <summary>
        /// 送检日期
        /// </summary>
        public DateTime? TestTime { get; set; }

        public DateTime? F_CreatorTime { get; set; }


        public void Configure(EntityTypeBuilder<ICU_LisInfo> entityBuilder, DbContext dbContext, Type dbContextLocator)
        {
            entityBuilder.HasKey(e => e.F_Id);

            entityBuilder.Property(e => e.F_Id).HasMaxLength(50);
            entityBuilder.Property(e => e.F_CreatorTime).HasColumnType("datetime");
            entityBuilder.Property(e => e.InpNo)
                .IsRequired()
                .HasMaxLength(20)
                .HasComment("住院号");
            entityBuilder.Property(e => e.ItemCode)
                .HasMaxLength(20)
                .HasComment("检验项目编码");
            entityBuilder.Property(e => e.ItemName)
                .HasMaxLength(100)
                .HasComment("检验项目");
            entityBuilder.Property(e => e.Name)
                .HasMaxLength(20)
                .HasComment("姓名");
            entityBuilder.Property(e => e.PatientID)
                .IsRequired()
                .HasMaxLength(20)
                .HasComment("患者编号");
            entityBuilder.Property(e => e.Result).HasMaxLength(50);
            entityBuilder.Property(e => e.ResultUnit).HasMaxLength(20);
            entityBuilder.Property(e => e.Sex)
                .HasMaxLength(20)
                .HasComment("性别");
            entityBuilder.Property(e => e.StdValue)
                .HasMaxLength(50)
                .HasComment("参照值");
            entityBuilder.Property(e => e.TestItemCode)
                .HasMaxLength(50)
                .HasComment("具体检验编码");
            entityBuilder.Property(e => e.TestItemName)
                .HasMaxLength(50)
                .HasComment("具体检验项");
            entityBuilder.Property(e => e.TestTime)
                .HasComment("送检日期")
                .HasColumnType("datetime");
        }
    }
}
