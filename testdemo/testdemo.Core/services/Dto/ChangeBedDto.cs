﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace testdemo.Core.services
{
    public class ChangeBedDto
    {
        [Required]
        public string InpNo { get; set; }

        [Required]
        public string BedNo { get; set; }

    }
}
