﻿
using Furion.DatabaseAccessor;
using Furion.DatabaseAccessor.Extensions;
using Furion.DependencyInjection;
using Furion.DynamicApiController;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using testdemo.Core.Entity;
using testdemo.Core.Locator;
using testdemo.Core.services;
using Yitter.IdGenerator;

namespace testdemo.Core.Service
{

    [ApiDescriptionSettings(Tag = "定时任务")]
    public class SaisSyncService : IDynamicApiController, ITransient
    {
        private readonly IServiceProvider _serviceProvider;
        private readonly IRepository<U_PhysiologicalData> _logicalData;
        private readonly IRepository<ICU_LisInfo, IcuDbContextLocator> _lisrepository;
        public SaisSyncService
            (
            IServiceProvider serviceProvider,
             IRepository<U_PhysiologicalData> logicalData,
             IRepository<ICU_LisInfo, IcuDbContextLocator> lisrepository
            )
        {
            _serviceProvider = serviceProvider;
            _logicalData = logicalData;
            _lisrepository = lisrepository;
        }

        /// <summary>
        /// 查询最后一条数据
        /// </summary>
        /// <returns></returns>
        public async Task<Object> GetIcuList()
        {
            var data = await _lisrepository.AsQueryable(false).OrderByDescending(i => i.F_Id).FirstOrDefaultAsync();
            return data;
        }


        /// <summary>
        /// 定时任务更新数据
        /// </summary>
        /// <returns></returns>
        public async Task UpdIcuList()
        {
            var data = await _lisrepository.AsQueryable(false).OrderByDescending(i => i.F_Id).FirstOrDefaultAsync();

            data.F_CreatorTime = DateTime.Now;

            await _lisrepository.UpdateAsync(data);// 用now可以更新
        }



        /// <summary>
        /// 同步手术间数据
        /// </summary>
        /// <returns></returns>
        [ApiDescriptionSettings(false)]
        public async Task AsyncOperaRoomData(IServiceProvider services)
        {

            var dataTable = "select * from L_Room where status =0 and RoomOrPACU =0 order by id".SetContextScoped(services).SqlQuery().Rows;

            if (dataTable.Count <= 0)
                return;

            DateTime now = DateTime.Now;
            DateTime startTime = new DateTime(now.Year, now.Month, now.Day, now.Hour, now.Minute, 0);
            DateTime endTime = new DateTime(now.Year, now.Month, now.Day, now.Hour, now.Minute, 59);

            var ListInsData = new List<U_PhysiologicalData>();
            for (int i = 0; i < dataTable.Count; i++)
            {

                var tableA = "U_PhysiologicalData_A_" + dataTable[i]["RoomName"];
                var tableM = "U_PhysiologicalData_M_" + dataTable[i]["RoomName"];

                string checkdata = startTime.AddMinutes(-2).ToString();

                var isHave = string.Format(@"select count(ID) count from {0} where CreateTime >='{1}' ", tableM, checkdata)
                    .Change<OtherDbContextLocator>().SetContextScoped(services).SqlQuery();

                if (Convert.ToInt32(isHave.Rows[0]["count"]) == 0)
                    continue;


                var sql = string.Format(@"select top 4 * from {0} where CreateTime >='{2}' and CreateTime <='{3}' order by CreateTime desc;
                    select top 4 * from {1} where CreateTime >='{2}' and CreateTime <='{3}' order by CreateTime desc;", tableM, tableA, checkdata, endTime);

                var data = await sql.Change<OtherDbContextLocator>().SetContextScoped(services).SqlQueriesAsync();

                var dataRowM = data.Tables[0].Rows;
                var dataRowA = data.Tables[1].Rows;

                var InsData = new U_PhysiologicalData();

                foreach (DataRow rowM in dataRowM)
                {
                    InsData.Hr = InsData.Hr == null ? rowM.Field<double>("Hr") != 0 ? rowM.Field<double>("Hr") : 0 : InsData.Hr;
                    InsData.Pulse = InsData.Pulse == null ? rowM.Field<double>("Pulse") != 0 ? rowM.Field<double>("Pulse") : 0 : InsData.Pulse;

                    InsData.NumberBreaths = InsData.NumberBreaths == null ? rowM.Field<double>("NumberBreaths") != 0 ?
                        rowM.Field<double>("NumberBreaths") : 0 : InsData.NumberBreaths;
                    InsData.SystolicPressure = InsData.SystolicPressure == null ? rowM.Field<double>("SystolicPressure") != 0 ?
                        rowM.Field<double>("SystolicPressure") : 0 : InsData.SystolicPressure;
                    InsData.DiastolicPressure = InsData.DiastolicPressure == null ? rowM.Field<double>("DiastolicPressure") != 0 ?
                        rowM.Field<double>("DiastolicPressure") : 0 : InsData.DiastolicPressure;

                    InsData.InvasiveSystolicPressure = InsData.InvasiveSystolicPressure == null ? rowM.Field<double>("InvasiveSystolicPressure") != 0
                        ? rowM.Field<double>("InvasiveSystolicPressure") : 0 : InsData.InvasiveSystolicPressure;

                    InsData.InvasiveDiastolicPressure = InsData.InvasiveDiastolicPressure == null ? rowM.Field<double>("InvasiveDiastolicPressure") != 0
                        ? rowM.Field<double>("InvasiveDiastolicPressure") : 0 : InsData.InvasiveDiastolicPressure;

                    InsData.Temperature = InsData.Temperature == null ? rowM.Field<double>("Temperature") != 0 ? rowM.Field<double>("Temperature") : 0 : InsData.Temperature;

                    InsData.EtCO2 = InsData.EtCO2 == null ? rowM.Field<double>("EtCO2") != 0 ? rowM.Field<double>("EtCO2") : 0 : InsData.EtCO2;
                    InsData.SpO2 = InsData.SpO2 == null ? rowM.Field<double>("SpO2") != 0 ? rowM.Field<double>("SpO2") : 0 : InsData.SpO2;
                    InsData.CVP = InsData.CVP == null ? rowM.Field<double>("CVP") != 0 ? rowM.Field<double>("CVP") : 0 : InsData.CVP;
                    InsData.BIS = InsData.BIS == null ? rowM.Field<double>("BIS") != 0 ? rowM.Field<double>("BIS") : 0 : InsData.BIS;
                }

                foreach (DataRow rowA in dataRowA)
                {
                    //InsData.EtCO2 = InsData.EtCO2 == null ? rowA.Field<double>("EtCO2") != 0 ? rowA.Field<double>("EtCO2") : 0 : InsData.EtCO2;
                    InsData.NumberBreaths = InsData.NumberBreaths == null ? rowA.Field<double>("NumberBreaths") != 0 ? rowA.Field<double>("NumberBreaths") : 0 : InsData.NumberBreaths;
                    InsData.VT = InsData.VT == null ? rowA.Field<double>("VT") != 0 ? rowA.Field<double>("VT") : 0 : InsData.VT;
                    InsData.Peak = InsData.Peak == null ? rowA.Field<double>("Peak") != 0 ? rowA.Field<double>("Peak") : 0 : InsData.Peak;
                    InsData.FiO2 = InsData.FiO2 == null ? rowA.Field<double>("FiO2") != 0 ? rowA.Field<double>("FiO2") : 0 : InsData.FiO2;
                    InsData.QdPressure = InsData.QdPressure == null ? rowA.Field<double>("QdPressure") != 0 ? rowA.Field<double>("QdPressure") : 0 : InsData.QdPressure;
                    InsData.JsValue = InsData.JsValue == null ? rowA.Field<double>("JsValue") != 0 ? rowA.Field<double>("JsValue") : 0 : InsData.JsValue;
                }

                string dateStart = startTime.AddMinutes(-5).ToString();
                if (InsData.SystolicPressure == 0)
                {
                    var Systolic = @"Select top 1 SystolicPressure from  U_PhysiologicalData     
                                                        where SystolicPressure!=0 and RoomName=@RoomName and CreateTime>=@dateStart and CreateTime<=@endTime order by CreateTime desc"
                                                        .SetContextScoped(services)
                                                        .SqlQuery(new { RoomName = dataTable[i]["RoomName"], dateStart, endTime });
                    InsData.SystolicPressure = Systolic.Rows.Count > 0 ? Convert.ToDouble(Systolic.Rows[0]["SystolicPressure"]) : 0;
                }

                if (InsData.DiastolicPressure == 0)
                {
                    var Diastolic = @"Select top 1 DiastolicPressure from U_PhysiologicalData 
                                                       where DiastolicPressure!=0 and RoomName=@RoomName and CreateTime>=@dateStart and CreateTime<=@endTime order by CreateTime desc"
                                                       .SetContextScoped(services)
                                                       .SqlQuery(new { RoomName = dataTable[i]["RoomName"], dateStart, endTime });
                    InsData.DiastolicPressure = Diastolic.Rows.Count > 0 ? Convert.ToDouble(Diastolic.Rows[0]["DiastolicPressure"]) : 0;
                }

                InsData.ID = YitIdHelper.NextId().ToString();
                InsData.RoomName = dataTable[i]["RoomName"].ToString();
                InsData.State = 0;
                InsData.CreateTime = DateTime.Now;
                ListInsData.Add(InsData);
            }

            if (ListInsData.Count > 0)
                await _logicalData.InsertNowAsync(ListInsData);

        }




        /// <summary>
        /// 保留麻醉监护前1万条数据
        /// </summary>
        /// <returns></returns>
        public async Task AsyncDelMAData(IServiceProvider services)
        {
            var dataTable = "select * from L_Room where status =0 and RoomOrPACU =0 order by id".SetContextScoped(services).SqlQuery().Rows;

            if (dataTable.Count <= 0)
                return;
            var sql = "";
            for (int i = 0; i < dataTable.Count; i++)
            {
                var tableA = "U_PhysiologicalData_A_" + dataTable[i]["RoomName"];
                var tableM = "U_PhysiologicalData_M_" + dataTable[i]["RoomName"];

                sql += string.Format(@" DELETE FROM {0}
                                                WHERE id NOT IN (
                                                SELECT TOP 10000 id
                                                FROM {0}
                                                ORDER BY CreateTime DESC
                                            );
                                           DELETE FROM {1}
                                                WHERE id NOT IN (
                                                SELECT TOP 10000 id
                                                FROM {1}
                                                ORDER BY CreateTime DESC
                                            );", tableM, tableA);
            }
            await sql.Change<OtherDbContextLocator>().SetContextScoped(services).SqlNonQueryAsync();
        }

    }
}
