﻿using Furion.DatabaseAccessor;
using Furion.DatabaseAccessor.Extensions;
using Furion.DependencyInjection;
using Furion.Schedule;
using Furion.TimeCrontab;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using testdemo.Core.Service;

namespace testdemo.Core.Jop
{
    [JobDetail("job6", Description = "定时任务更新数据", GroupName = "default", Concurrent = false)]
    [Cron("0 0 6 1/1 * ?", CronStringFormat.WithSeconds)]
    public class AsyncUdpMAData : IJob
    {
        private readonly ILogger<AsyncUdpMAData> _logger;
        private readonly IServiceProvider _serviceProvider;
        public AsyncUdpMAData(ILogger<AsyncUdpMAData> logger, IServiceProvider serviceProvider)
        {
            _logger = logger;
            _serviceProvider = serviceProvider;
        }

        public async Task ExecuteAsync(JobExecutingContext context, CancellationToken stoppingToken)
        {

            // 异步
            await Scoped.CreateAsync(async (factory, scope) =>
            {
                var services = scope.ServiceProvider;

                var dbContext = Db.GetDbContext(services);

                var otherService = services.GetService<SaisSyncService>();

                await otherService.UpdIcuList();

                dbContext.SaveChanges();
            });

            //using var serviceScope = _serviceProvider.CreateScope();

            //var services = serviceScope.ServiceProvider;

            //var dbContext = Db.GetDbContext(services);

            //var otherService = services.GetService<SaisSyncService>();

            //await otherService.UpdIcuList();

            //dbContext.SaveChanges();




            ////如果我下面的代码是在一个方法中，应该怎么传参数，我现在就是用的上面的方法传的services

            //var dataTable = "select * from L_Room where status =0 and RoomOrPACU =0 order by id".SetContextScoped(scope.ServiceProvider).SqlQuery().Rows;
            //if (dataTable.Count <= 0)
            //    return;

            //var sql = "";
            //for (int i = 0; i < dataTable.Count; i++)
            //{
            //    var tableA = "U_PhysiologicalData_A_" + dataTable[i]["RoomName"];
            //    var tableM = "U_PhysiologicalData_M_" + dataTable[i]["RoomName"];

            //    sql += string.Format(@" DELETE FROM {0}
            //                                WHERE id NOT IN (
            //                                SELECT TOP 10000 id
            //                                FROM {0}
            //                                ORDER BY CreateTime DESC
            //                            );
            //                           DELETE FROM {1}
            //                                WHERE id NOT IN (
            //                                SELECT TOP 10000 id
            //                                FROM {1}
            //                                ORDER BY CreateTime DESC
            //                            );"
            //    , tableM, tableA);
            //}
            ////sql.Change<OtherDbContextLocator>().SetContextScoped(scope.ServiceProvider).SqlNonQuery();
            //await sql.Change<OtherDbContextLocator>().SetContextScoped(scope.ServiceProvider).SqlNonQueryAsync();


            await Task.CompletedTask;
        }
    }
}
