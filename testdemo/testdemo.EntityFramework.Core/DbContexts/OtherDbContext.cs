﻿using Furion.DatabaseAccessor;
using Microsoft.EntityFrameworkCore;
using testdemo.Core;

namespace SAIS.EntityFramework.Core
{
    [AppDbContext("SAISPHYDB", DbProvider.SqlServer)]
    public class OtherDbContext : AppDbContext<OtherDbContext, OtherDbContextLocator>
    {
        public OtherDbContext(DbContextOptions<OtherDbContext> options) : base(options)
        {
            InsertOrUpdateIgnoreNullValues = true;
        }
    }
}
