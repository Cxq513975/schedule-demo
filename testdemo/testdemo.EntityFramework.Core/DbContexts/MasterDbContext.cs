﻿using Furion.DatabaseAccessor;
using Microsoft.EntityFrameworkCore;

namespace testdemo.EntityFramework.Core
{
    [AppDbContext("SAISDB", DbProvider.SqlServer)]
    public class MasterDbContext : AppDbContext<MasterDbContext>
    {
        public MasterDbContext(DbContextOptions<MasterDbContext> options) : base(options)
        {
            InsertOrUpdateIgnoreNullValues = true;
        }
    }
}