﻿using Furion.DatabaseAccessor;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace testdemo.EntityFramework.Core
{
    [AppDbContext("ICIMSDB", DbProvider.SqlServer)]
    public class IcuDbContext : AppDbContext<IcuDbContext>
    {
        public IcuDbContext(DbContextOptions<IcuDbContext> options) : base(options)
        {
            //InsertOrUpdateIgnoreNullValues = true;
        }
    }
}
