﻿using Furion;
using Furion.DatabaseAccessor;
using Microsoft.Extensions.DependencyInjection;
using SAIS.EntityFramework.Core;
using testdemo.Core;
using testdemo.Core.Locator;

namespace testdemo.EntityFramework.Core
{
    public class Startup : AppStartup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDatabaseAccessor(options =>
            {
                options.AddDbPool<MasterDbContext>(DbProvider.SqlServer);

                options.AddDbPool<OtherDbContext, OtherDbContextLocator>(DbProvider.SqlServer);

                options.AddDbPool<IcuDbContext, IcuDbContextLocator>(DbProvider.SqlServer);
            });
        }
    }
}