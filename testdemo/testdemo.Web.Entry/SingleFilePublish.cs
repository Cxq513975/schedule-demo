﻿using Furion;
using System.Reflection;

namespace testdemo.Web.Entry
{
    public class SingleFilePublish : ISingleFilePublish
    {
        public Assembly[] IncludeAssemblies()
        {
            return Array.Empty<Assembly>();
        }

        public string[] IncludeAssemblyNames()
        {
            return new[]
            {
            "testdemo.Application",
            "testdemo.Core",
            "testdemo.EntityFramework.Core",
            "testdemo.Web.Core"
        };
        }
    }
}