﻿namespace testdemo.Application
{
    public interface ISystemService
    {
        string GetDescription();
    }
}