﻿using Furion;
using Furion.Schedule;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace testdemo.Web.Core
{
    public class Startup : AppStartup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddConsoleFormatter();
            services.AddJwt<JwtHandler>();

            services.AddCorsAccessor();

            services.AddControllers()
                    .AddInjectWithUnifyResult();

            services.AddSchedule(options =>
            {
                options.LogEnabled = true;
                options.JobDetail.LogEnabled = false;
                options.AddJob(App.EffectiveTypes.ScanToBuilders());
            });
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseCorsAccessor();

            app.UseAuthentication();
            app.UseAuthorization();


            // 任务调度看板
            app.UseScheduleUI();

            // 还可以配置生产环境关闭
            app.UseScheduleUI(options =>
            {
                // 此操作暂提供 URL 重写功能，也就是会自动跳转
                options.RequestPath = "/comjob";  // Furion 4.8.5.6+ 版本支持，必须以 / 开头且不以 / 结尾
                options.DisableOnProduction = false;
            });

            app.UseInject(string.Empty);

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}